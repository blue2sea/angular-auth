(function(){
    function route_provider($routeProvider) {
        $routeProvider
            .when('/', {
                    templateUrl: 'home.html',
                    controller: 'homeController'
                }
            )
    }

    function home_ctrl($scope) {
        $scope.message = 'Welcome';
    }

    angular.module('app', ['ngRoute'])
        .config(['$routeProvider', route_provider])
        .controller('homeController', home_ctrl)

})();