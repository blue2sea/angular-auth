# Errors occured in AngularJS Development and Way to fix errors

## The way to fix 'Unknown provider' error
This error occurs when controller function has user-defined parameter, and service function for this parameter
is not defined.
For example;

    function MainCtrl(user) {
        var self = this;

        self.login = function() {
            user.login()
        }
    }
    angular.module('app', [])
        .controller('Main', MainCtrl)

In the above code, the web browser makes error something like this

    Error: [$injector:unpr] Unknown provider: userProvider <- user <- Main         angular.1.6.1.js:14328
    http://errors.angularjs.org/1.6.1/$injector/unpr?p0=userProvider%20%3C-%20user%20%3C-%20Main
        at angular.1.6.1.js:68
        at angular.1.6.1.js:4630
        at Object.getService [as get] (angular.1.6.1.js:4783)
        at angular.1.6.1.js:4635
        at getService (angular.1.6.1.js:4783)
        at injectionArgs (angular.1.6.1.js:4808)
        at Object.invoke (angular.1.6.1.js:4834)
        at $controllerInit (angular.1.6.1.js:10695)
        at nodeLinkFn (angular.1.6.1.js:9572)
        at compositeLinkFn (angular.1.6.1.js:8881)

I added service function like this

    function userService($http, API) {
        var self = this;
        self.getQuote = function() {
            return $http.get(API + '/auth/quote')
        }
    }

    function MainCtrl(user) {
        var self = this;

        self.login = function() {
            user.login()
        }
    }

    angular.module('app', [])
        .service('user', userService)
        .controller('Main', MainCtrl)

Then, the web browser makes error something like this

    Error: [$injector:unpr] Unknown provider: APIProvider <- API <- user        angular.1.6.1.js:14328
    http://errors.angularjs.org/1.6.1/$injector/unpr?p0=APIProvider%20%3C-%20API%20%3C-%20user
        at angular.1.6.1.js:68
        at angular.1.6.1.js:4630
        at Object.getService [as get] (angular.1.6.1.js:4783)
        at angular.1.6.1.js:4635
        at getService (angular.1.6.1.js:4783)
        at injectionArgs (angular.1.6.1.js:4808)
        at Object.instantiate (angular.1.6.1.js:4854)
        at Object.<anonymous> (angular.1.6.1.js:4692)
        at Object.invoke (angular.1.6.1.js:4842)
        at Object.enforcedReturnValue [as $get] (angular.1.6.1.js:4676)

I removed parameter 'API' in userService, and there is no error.
Or, declare 'API' as constant is another way, like this

    angular.module('app', [])
        .service('user', userService)
        .constant('API', 'http://test-routes.herokuapp.com')
        .controller('Main', MainCtrl)


## The way to fix "Argument 'fn' is not a function, got string" error
This error occurs when angular.module uses config structure like this.

    function route_provider($routeProvider) {        
    }
    angular.module('app', ['ngRoute'])
           .config('$routeProvider', route_provider)

Error message is like this.

    Uncaught Error: [$injector:modulerr] Failed to instantiate module app due to:
    Error: [ng:areq] Argument 'fn' is not a function, got string
    ...
    
The way to fix this error is 3 step.

    1.  Use .config(['$routeProvider', route_provider]) instead of .config('$routeProvider', route_provider) 
        should use []
    2.  Use ['ngRoute'] in angular.module function. 
        If no params in [], the 'Unknown provider: $routeProvider' error occurs
    3.  Use $ signature with routeProvider param.
        If you use routeProvider instead of $routeProvider, 'Unknown provider: $routeProvider' error occurs

The above 3 conditions is satisfied, the error is solved














